package pl.strefatestowa.nodetask;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;

public class MyStructureTest {

    private static Node node1 = new Node("valueC1", "valueR1");
    private static CompositeNode compositeNode2 = new CompositeNode("valueC2", "valueR2");
    private static Node node3 = new Node("valueC3", "valueR3");
    private static CompositeNode compositeNode4 = new CompositeNode("valueC4", "valueR4");
    private static CompositeNode compositeNode5 = new CompositeNode("valueC5", "valueR5");
    private static Node node6 = new Node("valueC6", "valueR6");
    private static CompositeNode compositeNode7 = new CompositeNode("valueC7", "valueR7");
    private static Node node8 = new Node("valueC8", "valueR8");

    private MyStructure testStructure;

    @BeforeEach
    void setUp() {
        testStructure = new MyStructure();

        compositeNode2.addNode(node3);
        compositeNode2.addNode(compositeNode4);
        compositeNode4.addNode(compositeNode5);
        compositeNode5.addNode(node6);
        compositeNode7.addNode(node8);

        testStructure.addNode(node1);
        testStructure.addNode(compositeNode2);
        testStructure.addNode(compositeNode7);
    }

    @Test
    void shouldReturnNullWhenNotFoundByCodeValue() {
        assertThat(testStructure.findByCode("abcd"), is(nullValue()));
    }

    @Test
    void shouldReturnNodeFoundByCodeValue() {
        assertThat(testStructure.findByCode("valueC1"), is(node1));
    }

    @Test
    void shouldReturnNullWhenNotFoundByRendererValue() {
        assertThat(testStructure.findByRenderer("abcd"), is(nullValue()));
    }

    @Test
    void shouldReturnNodeFoundByRendererValue() {
        assertThat(testStructure.findByRenderer("valueR1"), is(node1));
    }

    @Test
    void shouldFindCompositeNodeByCodeValue() {
        assertThat(testStructure.findByCode("valueC4"), is(compositeNode4));
    }

    @Test
    void shouldFindCompositeNodeByRendererValue() {
        assertThat(testStructure.findByRenderer("valueR4"), is(compositeNode4));
    }

    @Test
    void shouldBeAbleToFindNestedNodeByCodeValue() {
        assertThat(testStructure.findByCode("valueC6"), is(node6));
    }

    @Test
    void shouldBeAbleToFindNestedNodeByRendererValue() {
        assertThat(testStructure.findByRenderer("valueR6"), is(node6));
    }

    @Test
    void shouldReturnCorrectNodesNumber() {
        assertThat(testStructure.count(), is(8));
    }
}

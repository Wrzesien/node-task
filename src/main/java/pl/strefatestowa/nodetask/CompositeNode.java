package pl.strefatestowa.nodetask;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class CompositeNode extends Node implements ICompositeNode {

    private List<INode> nodes = new LinkedList<>();

    public CompositeNode(String code, String renderer) {
        super(code, renderer);
    }

    @Override
    public List<INode> getNodes() {
        return Collections.synchronizedList(nodes);
    }

    public void addNode(INode node) {
        nodes.add(node);
    }

//    metoda dodaje węzeł wraz z zagnieżdżonymi węzłami do strumienia
    @Override
    public Stream<INode> toStream() {
        return Stream.concat(super.toStream(), nodes.stream().flatMap(INode::toStream));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompositeNode)) return false;
        if (!super.equals(o)) return false;
        CompositeNode that = (CompositeNode) o;
        return Objects.equals(getNodes(), that.getNodes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getNodes());
    }
}

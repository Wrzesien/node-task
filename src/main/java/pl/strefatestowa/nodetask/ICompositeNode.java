package pl.strefatestowa.nodetask;

import java.util.List;

interface ICompositeNode extends INode {

    List<INode> getNodes();
}

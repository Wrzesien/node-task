package pl.strefatestowa.nodetask;

import java.util.Objects;
import java.util.stream.Stream;

public class Node implements INode{

    private String code;
    private String renderer;

    public Node(String code, String renderer) {
        this.code = code;
        this.renderer = renderer;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getRenderer() {
        return renderer;
    }

//    metoda dodaje węzeł do strumienia
    @Override
    public Stream<INode> toStream() {
        return Stream.of(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return Objects.equals(getCode(), node.getCode()) &&
                Objects.equals(getRenderer(), node.getRenderer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode(), getRenderer());
    }
}

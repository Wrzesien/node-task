package pl.strefatestowa.nodetask;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class MyStructure implements IMyStructure {

    private List<INode> nodes = new LinkedList<>();

    public INode findByCode(String code) {
        if (code == null) {
            throw new IllegalArgumentException("You didn't provide any value");
        }
        return findByLambdaExpression(n -> code.equals(n.getCode()));
    }

    public INode findByRenderer(String renderer) {
        if (renderer == null) {
            throw new IllegalArgumentException("You didn't provide any value");
        }
        return findByLambdaExpression(n -> renderer.equals(n.getRenderer()));
    }

    public int count() {
        return (int) nodes.stream().flatMap(INode::toStream).count();
    }

    //    metoda pomocnicza, dzięki której unika się powielania kodu w metodach findByCode() i findByRenderer()
    private INode findByLambdaExpression(Predicate<INode> expression) {
        return nodes.stream()
                .flatMap(INode::toStream)
                .filter(expression)
                .findFirst()
                .orElse(null);
    }

    public void addNode(INode node) {
        nodes.add(node);
    }
}
